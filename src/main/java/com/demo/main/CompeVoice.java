package com.demo.main;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.DefaultedHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author： wangxs
 * @date：Create by 2018/8/28 17:16
 */
public class CompeVoice {
    private static RequestConfig requestConfig =
            RequestConfig.custom().setSocketTimeout(10000).setConnectTimeout(10000).build();
    private static Logger logger = LoggerFactory.getLogger(CompeVoice.class);
    public static void main(String[] args) {
        String pathUrl = "http://192.168.88.23:8896/voice/ttsDataBaker";
        String encoding = "utf-8";
        String body = "{\"text\": \"这是什么玩意\"}";
        Map<String, Object> parames = new HashMap<String, Object>();
        parames.put("text","什么玩意");
        parames.put("name","ceshimingzi");
        String sr = JSON.toJSONString(parames);

        doPost(pathUrl,encoding, sr);

        /*String url = "http://192.168.88.23:8896/voice/ttsDataBaker";

        Map<String, Object> parames = new HashMap<String, Object>();
        Gson gson = new Gson();
        String json = null;
        try {
            parames.put("text", "这是什么玩意");

            json = gson.toJson(parames);

            System.out.println("parames:" + json);
            String receive = doPost(url, json);
            System.out.println("receive:" + receive);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }


    static void doPost(String url, String encoding, String params){
        String result = null;
        CloseableHttpResponse httpResp = null;
        CloseableHttpClient httpclient = null;
        try {
            HttpPost httpPost = new HttpPost(url);
            httpPost.addHeader(HTTP.CONTENT_TYPE, "application/json; charset=UTF-8");

            StringEntity se = new StringEntity(params, "utf-8");
            httpPost.setEntity(se);
            //设置请求和传输超时时间
            httpclient = HttpClients.createDefault();
            httpResp = httpclient.execute(httpPost);
            int statusCode = httpResp.getStatusLine().getStatusCode();
            if (statusCode == HttpStatus.SC_OK) {
                result = EntityUtils.toString(httpResp.getEntity(), encoding);
                logger.info("HttpGet方式请求成功！返回结果：{}", result);
            } else {
                logger.info("HttpGet方式请求失败！状态码:" + httpResp.getStatusLine().getStatusCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String doPost(String url, String postData) {
        String result = null;
        HttpPost post = null;
        try {
            HttpClient client = new DefaultHttpClient();

            post = new HttpPost(url);

            post.setHeader(HTTP.CONTENT_TYPE, "application/json; charset=UTF-8");
            post.setHeader("Accept", "application/json; charset=UTF-8");

            StringEntity entity = new StringEntity(postData, "UTF-8");
            post.setEntity(entity);

            HttpResponse response = client.execute(post);

            int rspCode = response.getStatusLine().getStatusCode();
            System.out.println("rspCode:" + rspCode);
            result = EntityUtils.toString(response.getEntity(), "UTF-8");

            return result;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(post != null) {
                post.releaseConnection();
            }
        }
        return null;
    }
}
